# Status - Incomplete

# Mobile
* Activated by
    * Tap
    * _What about when used in an emulator/simulator?_

# Desktop
* Activated by
    * Click
    * Spacebar
* Searching
    * Typing slowly selects items by matching the first character.  Subsequent keypresses after the debounce timeout will select the next matching item.
    * Typing quickly will perform a substring search from the beginning of the item's text value.
* Selection
    * Click
    * Enter key