# UI Component Test Suite

The project documents capabilities that should be present in custom versions of common UI components.

## Motivation

System widgets are like icebergs.  Most of the functionality is hidden beneath the surface.  Far too many times when we encounter custom components only the tip of the iceberg has been implemented which leads to a frustrating experience and often one that doesn't account for the variety of devices that may be used to render the component.

I would like to make it easy to see the effort required to mimic system widgets properly and hopefully steer people away from it unless they are committed to making the investment of time to deliver high quality user experiences.
